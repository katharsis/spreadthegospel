# -*- coding: utf-8 -*-

import string
from random import choice

from django.db import models

from core.utils import _slugify
from urlparse import urlparse

from django.core.mail import EmailMessage

from django.core.exceptions import ValidationError

from django.contrib.sitemaps import ping_google

import feedparser

class News(models.Model):
    author = models.CharField(verbose_name = u"Imię i nazwisko/Nick", max_length = 200)
    title = models.CharField(verbose_name = u"Tytuł", max_length = 200)
    text = models.TextField(verbose_name = u"Tekst")
    availability = models.BooleanField(verbose_name = u"Dostępność", default = True)
    url = models.SlugField(verbose_name = u"Adres url", editable = False, max_length = 200)
    href = models.URLField(verbose_name = u"Hiperłącze", editable = False, max_length = 200)
    date = models.DateTimeField(verbose_name = u"Data publikacji")

    class Meta:
        verbose_name = u"News"
        verbose_name_plural = u"newsy"

    def save(self, *args, **kwargs):
        if not self.id:
            super(News, self).save(*args, **kwargs)

        self.url = "%s-%s.html" % (_slugify(self.title), self.id)

        super(News, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/%s" % self.url

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.title)

class Subpage(models.Model):
    title = models.CharField(verbose_name = u"Tytuł", max_length = 150, unique = True)
    url = models.SlugField(verbose_name = u"URL", max_length = 150, editable = False)
    text = models.TextField(verbose_name = u"Treść", blank = True, null = True)
    modules = ((-1, u"Standardowa"), (0, u"Formularz zgłoszeniowy"), (1, u"Formularz kontaktowy"))
    selectedModule = models.IntegerField(verbose_name = u"Moduł", max_length = 150, choices = modules, default = -1)
    date = models.DateTimeField(verbose_name = u"Data ostatniej edycji", auto_now = True)

    def mod(self):
        return self.modules[self.selectedModule + 1][1]

    mod.allow_tags = True
    mod.short_description = u"Moduł"

    class Meta:
        verbose_name = u"Podstrona"
        verbose_name_plural = u"podstrony"

    def save(self, *args, **kwargs):
        self.url = _slugify(self.title)

        super(Subpage, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        return (u"subpage", (), {"subpage": self.url})

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.title)

class NewsFeed(models.Model):
    author = models.CharField(verbose_name = u"Imię i nazwisko/Nick", max_length = 100)
    email = models.EmailField(verbose_name = u"E-mail")
    source = models.URLField(verbose_name = u"RSS/ATOM", unique = True)
    availability = models.BooleanField(verbose_name = u"Dostępność", default = False)
    sendmail = models.BooleanField(verbose_name = u"Potwierdzenie", default = True)

    class Meta:
        verbose_name = u"Kanał"
        verbose_name_plural = u"kanały"

    def clean(self, *args, **kwargs):
        if not feedparser.parse(self.source).version:
            raise ValidationError(u"Niestandardowy format kanału. Zalecana architektura: RSS 1.0, RSS 2.0 lub ATOM 1.0.")

        super(NewsFeed, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.availability and self.sendmail:
            title = u"pl.python.org/planeta - potwierdzenie akceptacji bloga"
            text = u"Witaj,<br><br>Informujemy, że Twój <a href=\"http://%s\" title=\"%s\">blog</a> został zweryfikowany i dodany do <a href=\"http://pl.python.org/planeta\" title=\"Polska planeta programistów Python\">polskiej planety programistów Python</a>.<br><br>Z poważaniem,<br>Polish Python Coders Group<br><a href=\"mailto:ppcg@pl.python.org\" title=\"ppcg@pl.python.org\">ppcg@pl.python.org</a>" % (urlparse(self.source).hostname, self.author)
            who = u"PPCG Planeta <planeta@pl.python.org>"

            msg = EmailMessage(title, text, who, [self.email])
            msg.content_subtype = "html"
            msg.send()

        super(NewsFeed, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.sendmail:
            title = u"pl.python.org/planeta - potwierdzenie usunięcia bloga"
            text = u"Witaj,<br><br>Informujemy, że Twój <a href=\"http://%s\" title=\"%s\">blog</a> został usunięty z <a href=\"http://pl.python.org/planeta\" title=\"Polska planeta programistów Python\">polskiej planety programistów Python</a>.<br><br>Z poważaniem,<br>Polish Python Coders Group<br><a href=\"mailto:ppcg@pl.python.org\" title=\"ppcg@pl.python.org\">ppcg@pl.python.org</a>" % (urlparse(self.source).hostname, self.author)
            who = u"PPCG Planeta <planeta@pl.python.org>"

            msg = EmailMessage(title, text, who, [self.email])
            msg.content_subtype = "html"
            msg.send()

        super(NewsFeed, self).delete(*args, **kwargs)

    def __str__(self):
        return str(self.author)

    def __unicode__(self):
        return unicode(self.author)

class Subscriber(models.Model):
    email = models.EmailField(verbose_name = u"E-mail", unique = True)
    data = models.DateField(auto_now = True)
    confirmation = models.BooleanField(default = False)
    activation_key = models.CharField(max_length = 40, editable = False)

    def save(self, *args, **kwargs):
        size = 40

        if not self.pk:
            self.activation_key = "".join([choice(string.letters + string.digits) for x in range(size)])

        super(Subscriber, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u"Subskrybent"
        verbose_name_plural = u"subskrybenci"

    def delete(self, *args, **kwargs):
        if self.confirmation:
            title = u"pl.python.org/planeta - potwierdzenie usunięcia z subskrypcji"
            text = u"Witaj,<br><br>Dzięki za dotychczasową subskrypcję.<br><br>Pamiętaj, że w każdej chwili możesz dokonać ponownej rejestracji, wypełniając formularz zgłoszeniowy na stronie <a href=\"http://pl.python.org/planeta\" title=\"Polska planeta programistów Python\">polskiej planety programistów Python</a><br><br>Z poważaniem,<br>Polish Python Coders Group<br><a href=\"mailto:ppcg@pl.python.org\" title=\"ppcg@pl.python.org\">ppcg@pl.python.org</a>"
            who = u"PPCG Planeta <planeta@pl.python.org>"

            msg = EmailMessage(title, text, who, [self.email])
            msg.content_subtype = "html"
            msg.send()

        super(Subscriber, self).delete(*args, **kwargs)

    def __str__(self):
        return str(self.email)

    def __unicode__(self):
        return unicode(self.email)

class JobOffer(models.Model):
    topic_id = models.SmallIntegerField(editable = False, unique = True, default = 0)
    city = models.CharField(verbose_name = u"Miejsce pracy", max_length = 150)
    company = models.CharField(verbose_name = u"Nazwa firmy", max_length = 150)
    title = models.CharField(verbose_name = u"Stanowisko pracy", max_length = 150)
    href = models.URLField(verbose_name = u"Hiperłącze", max_length = 150, blank = True, null = True)
    date = models.DateField(verbose_name = u"Data publikacji")

    def save(self, *args, **kwargs):
        super(JobOffer, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u"Oferta pracy"
        verbose_name_plural = u"oferty pracy"

    def __str__(self):
        return str(self.topic_id)

    def __unicode__(self):
        return unicode(self.topic_id)
