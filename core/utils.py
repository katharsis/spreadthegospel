# -*- coding: utf-8 -*-

try:
    import Image
except ImportError:
    from PIL import Image

try:
    import ImageDraw
    import ImageFont
except ImportError:
    pass

import cStringIO
import hashlib
import string

from random import choice
from random import randint

from django.conf import settings

from django.template.defaultfilters import slugify

def _slugify(text):
    return slugify(text.replace(u"ł", u"l").replace(u"Ł", u"L"))

def preparation_objects_to_move(qs, pk, parent_id = None):
    if not parent_id:
        objects = [obj.key for obj in qs.objects.all()]
    else:
        objects = [obj.key for obj in qs.objects.filter(parent = parent_id)]

    objects.sort()

    clicked_key_index = objects.index(int(pk))

    return (objects, clicked_key_index)

def template_objects_move(direction, qs, clicked_key_index, pk, model):
    if direction == "up":
        post_key = qs[clicked_key_index - 1]
    else:
        post_key = qs[clicked_key_index + 1]

    post_menu = model.objects.get(key = post_key)
    current_menu = model.objects.get(key = pk)

    post_menu.key, current_menu.key = current_menu.key, post_menu.key

    post_menu.save()
    current_menu.save()

def renderCapchaImage(width, height, length, fontSize):
    imgtext = "".join([choice(string.ascii_letters + string.digits + "-+!@%?&#$=") for x in xrange(length)])
    imghash = hashlib.sha1(imgtext).hexdigest()
    img = Image.new("RGB", (width, height), "#FFFFFF")
    draw = ImageDraw.Draw(img)

    r, g, b = randint(0, 255), randint(0, 255), randint(0, 255)
    dr = (randint(0, 255) - r) / 300
    dg = (randint(0, 255) - g) / 300
    db = (randint(0, 255) - b) / 300

    for x in xrange(300):
        r, g, b = r + dr, g + dg, b + db
        draw.line((x, 0, x, 300), fill = (r, g, b))

    font = ImageFont.truetype(settings.STATIC_FONT, fontSize)
    parm = draw.textsize(imgtext, font)
    draw.text(((width - parm[0]) * 0.5, (height - parm[1]) * 0.5), imgtext, font = font, fill = (255, 255, 255))

    captchaFile = cStringIO.StringIO()
    img.save(captchaFile, "PNG")
    captchaFile.seek(0)
    data = {"imghash" : imghash, "picture" : captchaFile.read()}

    return data
