# -*- coding: utf-8 -*-

from django.contrib import admin

from core.models import News
from core.models import Subpage
from core.models import NewsFeed
from core.models import Subscriber
from core.models import JobOffer

admin.site.disable_action("delete_selected")

def deleteNewsFeed(modeladmin, request, queryset):
    [x.delete() for x in queryset]

    numberOfDeleted = len(queryset)

    if numberOfDeleted == 1:
        info = u"Usunięto 1 kanał."
    elif 1 < numberOfDeleted < 5:
        info = u"Usunięto %d kanały." % numberOfDeleted
    else:
        info = u"Usunięto %d kanałów." % numberOfDeleted

    modeladmin.message_user(request, info)

deleteNewsFeed.short_description = u"Usuń wybrane kanały"

def deleteSubscriber(modeladmin, request, queryset):
    [x.delete() for x in queryset]

    numberOfDeleted = len(queryset)

    if numberOfDeleted == 1:
        info = u"Usunięto 1 subskrypcję."
    elif 1 < numberOfDeleted < 5:
        info = u"Usunięto %d subskrypcje." % numberOfDeleted
    else:
        info = u"Usunięto %d subskrypcji." % numberOfDeleted

    modeladmin.message_user(request, info)

deleteSubscriber.short_description = u"Usuń wybrane subskrypcje"

class News_Admin(admin.ModelAdmin):
    list_per_page = 15
    list_display = ("title", "author", "availability", "date")
    list_filter = ["author", "availability"]

    search_fields = ("title", "author",)

    class Media:
        js = ["/statics/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js", "/statics/js/tinymce_setup.js"]

    def queryset(self, request):
        qs = super(News_Admin, self).queryset(request)

        return qs.order_by("-date")

    actions = ["delete_selected"]

class Subpage_Admin(admin.ModelAdmin):
    list_display = ("title", "mod")
    list_filter = ["selectedModule"]

    search_fields = ("title",)

    class Media:
        js = ["/statics/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js", "/statics/js/tinymce_setup.js"]

    actions = ["delete_selected"]

class NewsFeed_Admin(admin.ModelAdmin):
    list_per_page = 15
    list_display = ("author", "source", "availability", "email")
    list_filter = ["availability"]

    search_fields = ("author", "source", "email",)

    actions = [deleteNewsFeed]

class Subscriber_Admin(admin.ModelAdmin):
    list_per_page = 15
    list_display = ("email", "confirmation", "data")
    list_filter = ["confirmation"]

    search_fields = ("email",)

    actions = [deleteSubscriber]

class JobOffer_Admin(admin.ModelAdmin):
    list_per_page = 15
    list_display = ("topic_id", "city", "company", "title", "date")
    list_filter = ["city", "company"]

    ordering = ("-date",)

    search_fields = ("city", "company", "title",)

    actions = ["delete_selected"]

admin.site.register(News, News_Admin)
admin.site.register(Subpage, Subpage_Admin)
admin.site.register(NewsFeed, NewsFeed_Admin)
admin.site.register(Subscriber, Subscriber_Admin)
admin.site.register(JobOffer, JobOffer_Admin)