# -*- coding: utf-8 -*-

from django import forms

import feedparser

from django.core.exceptions import ValidationError

def valid_feed(source):
    feed = feedparser.parse(source)

    try:
        if not feed.version:
            raise ValidationError(u"Niestandardowy format kanału. Zalecana architektura: RSS 1.0, RSS 2.0 lub ATOM 1.0.")
    except AttributeError:
        pass

class Newsletter(forms.Form):
    email = forms.EmailField(widget = forms.TextInput(attrs = {"id" : "newsletter_email", "title" : u"Wpisz adres e-mail"}))
    regulation = forms.BooleanField(required = True, initial = False, widget = forms.CheckboxInput(attrs = {"title" : u"Zaakceptuj regulamin"}))

class FeedRegister(forms.Form):
    author = forms.CharField(max_length = 100, widget = forms.TextInput(attrs = {"title" : u"Wpisz imię i nazwisko albo nick"}))
    email = forms.EmailField(widget = forms.TextInput(attrs = {"title" : u"Wpisz adres e-mail"}))
    source = forms.URLField(widget = forms.TextInput(attrs = {"title" : u"Wpisz adres kanału RSS lub ATOM"}), validators = [valid_feed])
    regulation = forms.BooleanField(required = True, initial = False, widget = forms.CheckboxInput(attrs = {"title" : u"Zaakceptuj regulamin", "id" : "id_regulation_sub"}))
    token = forms.CharField(max_length = 8, widget = forms.TextInput(attrs = {"title" : u"Przepisz token z obrazka"}))

class SearchForm(forms.Form):
    fraza = forms.CharField(max_length = 20, widget = forms.TextInput(attrs = {"id" : "search", "title" : "Wpisz szukaną frazę"}))

class ContactForm(forms.Form):
    author = forms.CharField(max_length = 20, widget = forms.TextInput(attrs = {"title" : u"Wpisz imię i nazwisko albo nick"}))
    email = forms.EmailField(widget = forms.TextInput(attrs = {"title" : u"Wpisz adres e-mail"}))
    topic = forms.CharField(max_length = 100, widget = forms.TextInput(attrs = {"title" : u"Wpisz tytuł wiadomości"}))
    message = forms.CharField(widget = forms.Textarea(attrs = {"rows" : 10, "cols" : 60, "title" : u"Wpisz treść wiadomości"}))
    token = forms.CharField(max_length = 8, widget = forms.TextInput(attrs = {"title" : u"Przepisz token z obrazka"}))    
