# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns
from django.conf.urls.defaults import url

from core.feeds import RSS
from core.feeds import Atom

urlpatterns = patterns("core.views",
    url(r"^$", "showPosts"),
    url(r"^feeds/rss/$", RSS(), name = "RSS"),
    url(r"^feeds/atom/$", Atom(), name = "Atom"),
    url(r"^kontakt/sukces/$", "showContactFormSuccessfully"),
    url(r"^kontakt/error/$", "showContactFormUnsuccessfully"),
    url(r"^zglos-bloga/sukces/$", "showRegisterFeedSuccessfully"),
    url(r"^zglos-bloga/error/$", "showRegisterFeedUnsuccessfully"),
    url(r"^zglos-bloga/error-uniq/$", "showRegisterFeedUnsuccessfullyUniq"),
    url(r"^newsletter/$", "showNewsletterForm"),
    url(r"^newsletter/sukces/$", "showRegisterNewsletterSuccessfully"),
    url(r"^newsletter/error/$", "showRegisterNewsletterUnsuccessfully"),
    url(r"^newsletter/(\w{40})$", "confirmNewsletter"),
    url(r"^news/(?P<number>[\w\-_]+)/$", "showPosts"),
    url(r"^szukaj/(?P<page>[\w\-_]+)/$", "showSearchResults", name = "search-results"),
    url(r"^(?P<slug>[\w\-_]+)\-(?P<pk>[\d]+)\.html$", "showSingleNews"),
    url(r"^(?P<subpage>[\w\-_]+)/$", "showSubpage", name = "subpage"),
    url(r"^captcha.png/(?P<url>[\w\-_]+)$", "captchaGenerator", name = "captcha"),
)
