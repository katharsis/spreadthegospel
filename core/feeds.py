# -*- coding: utf-8 -*-

from core.models import News

from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed

from django.utils.html import strip_tags
from SpreadTheGospel.settings import DOMAIN_NAME

class RSS(Feed):
    title = "Kanał informacyjny PPCG Planeta - pl.python.org/planeta"
    link = "/"
    description = "Kanał informacyjny PPCG Planeta - pl.python.org/planeta"

    def items(self):
        numberOfNews = 10

        return News.objects.filter(availability = True).order_by("-date")[:numberOfNews]

    def item_pubdate(self, item):
        return item.date

    def item_link(self, item):
        return "%s%s" % (DOMAIN_NAME, item.url)

    def item_description(self, item):
        return strip_tags(item.text)

class Atom(RSS):
    feed_type = Atom1Feed
    subtitle = RSS.description
