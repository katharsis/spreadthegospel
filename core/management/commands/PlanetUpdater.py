# -*- coding: utf-8 -*-

from django.core.management.base import NoArgsCommand

import feedparser

from datetime import datetime
from datetime import date
from datetime import time

from core.models import News
from core.models import NewsFeed

from config.models import Setup

class PlanetUpdater:
    def __init__(self):
        self.NewsFeed = NewsFeed.objects.filter(availability = True)
        self.config = Setup.objects.get_or_create(id = 1)[0]

    def parseFeed(self):
        for singleFeed in self.NewsFeed:
            source = feedparser.parse(singleFeed.source)
            version = source.version

            for news in source.entries:
                if u"rss" in version:
                    date_struct = news.published_parsed
                elif u"atom" in version:
                    date_struct = news.updated_parsed
            
                d = date(date_struct.tm_year, date_struct.tm_mon, date_struct.tm_mday)
                t = time(date_struct.tm_hour, date_struct.tm_min, date_struct.tm_sec)
            
                dt = datetime.combine(d, t)

                if self.config.update_data < dt and news.title not in [n.title for n in News.objects.filter(author = singleFeed.author)]:
                    title = news.title

                    if news.get("content"):
                        args = news.content[0]["value"]
                        
                    elif news.get("description"):
                        args = news.description
            
                    text = u"<br>".join([p.strip() for p in args.split("\n")])
                    text += u"<p class=\"source\">Źródło: <a href=\"%s\" title=\"%s\" rel=\"nofollow\">%s</a></p>" % (news.link, title, "%s..." % news.link[:90] if len(news.link) > 90 else news.link)

                    dt = dt.isoformat().split("T")
                    news = News(author = singleFeed.author, title = title, text = text, href = news.link, date = "%s %s" % (dt[0], dt[1]))
                    news.save()

    def update_data(self):
        self.config.update_data = datetime.now()
        self.config.save()

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        pu = PlanetUpdater()
        pu.parseFeed()
        pu.update_data()