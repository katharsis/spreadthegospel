# -*- coding: utf-8 -*-

from django.core.management.base import NoArgsCommand

import MySQLdb

from datetime import datetime
from datetime import timedelta

from SpreadTheGospel.settings import MySQL_PPCG_FORUM_HOST
from SpreadTheGospel.settings import MySQL_PPCG_FORUM_PORT
from SpreadTheGospel.settings import MySQL_PPCG_FORUM_USER
from SpreadTheGospel.settings import MySQL_PPCG_FORUM_PASSWORD
from SpreadTheGospel.settings import MySQL_PPCG_FORUM_DB 

from core.models import JobOffer

class JobOfferUpdater:
    def __init__(self):
        self.db = None
        self.jobs = None

    def conn(self, date):
        try:
            self.db = MySQLdb.connect(host = MySQL_PPCG_FORUM_HOST, port = MySQL_PPCG_FORUM_PORT, user = MySQL_PPCG_FORUM_USER, passwd = MySQL_PPCG_FORUM_PASSWORD, db = MySQL_PPCG_FORUM_DB)
            self.db.set_character_set("utf8")

            cursor = self.db.cursor()
            cursor.execute("SELECT ID_TOPIC, subject, FROM_UNIXTIME(modifiedTime) FROM forum_messages WHERE ID_BOARD = 9 AND FROM_UNIXTIME(modifiedTime) > \"%s\" GROUP BY 1 ORDER BY 1 DESC;" % date)

            self.jobs = cursor.fetchall()
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
        finally:
            if self.db:
                self.db.close()

    def addJobs(self):
        if self.jobs:
            for job in self.jobs:
                city = None
                company = None
                title = None

                try:
                    if "]" in job[1]:
                        part_1 = job[1].split("]")
                        city = part_1[0][1:]

                        part_2 = part_1[1].split(" - ")
                        company = part_2[0][1:]

                        title = part_2[1]
                except:
                    title = company
                    company = ""
                finally:
                    if job[0] not in [single_job.topic_id for single_job in JobOffer.objects.all()]:
                        if city and title or company:
                            jo = JobOffer(topic_id = job[0], city = city, company = company, title = title, date = job[2])
                            jo.save()

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        pjb = JobOfferUpdater()
        pjb.conn((datetime.now() - timedelta(days = 30)).strftime("%Y-%m-%d %H-%M-%S"))
        pjb.addJobs()
