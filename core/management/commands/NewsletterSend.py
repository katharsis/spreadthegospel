# -*- coding: utf-8 -*-

from django.core.management.base import NoArgsCommand

from django.core.mail import EmailMessage
from django.core.mail import get_connection

from core.models import News
from core.models import Subscriber
from core.models import JobOffer

from datetime import date
from datetime import datetime
from datetime import timedelta

class NewsletterSend:
    def __init__(self):
        self.Subscriber = Subscriber.objects.filter(confirmation = True)
        self.News = News.objects.filter(availability = True).filter(date__month = date.today().month).order_by("-date")
        self.JobOffer = JobOffer.objects.all().order_by("city")
        self.jobs = []

    def prepareNewsletter(self):
        self.url = "".join([u"<li><a href=\"%s\" title=\"%s\">%s</a></li>" % (news.href, news.title, news.title) for news in self.News])

        for job in self.JobOffer:
            if job.city and job.title or job.company:
                offer = u"[%s] %s - %s" % (job.city, job.company, job.title)
            else:
                offer = u"[%s] %s" % (job.city, job.title)
    
            listing = u"<li><a href=\"%s\" title=\"%s\">%s</a></li>" % (job.href if job.href else "http://pl.python.org/forum/index.php?topic=%d" % job.topic_id, offer, offer)
            self.jobs.append(listing)

        self.jobs = "".join(self.jobs)

    def sendEmail(self):
        email_base = [sub.email for sub in self.Subscriber]

        date_pre = datetime.now()
        date_post = (date_pre - timedelta(days = 30)).strftime("%Y-%m-%d")
        date_pre = date_pre.strftime("%Y-%m-%d")

        emails = []

        conn = get_connection()
        conn.open()

        for single_email in email_base:
            title = u"pl.python.org/planeta - newsletter [%s]" % date_pre
            text = u"Witaj,<br><br><strong>Newsletter obejmuje okres:</strong> od <u>%s</u> do <u>%s</u><br><br><strong>Aktualności z planety:</strong><ul>%s</ul><strong>Oferty pracy:</strong><ul>%s</ul>Z poważaniem,<br>Polish Python Coders Group<br><a href=\"mailto:ppcg@pl.python.org\" title=\"ppcg@pl.python.org\">ppcg@pl.python.org</a>" % (date_post, date_pre, self.url if self.url else u"<li>Brak nowych wpisów.</li>", self.jobs if self.jobs else u"<li>Brak nowych ofert.</li>")
            who = u"PPCG Planeta <planeta@pl.python.org>"

            msg = EmailMessage(title, text, who, [single_email])
            msg.content_subtype = "html"

            emails.append(msg)

        conn.send_messages(emails)
        conn.close()

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        n = NewsletterSend()
        n.prepareNewsletter()
        n.sendEmail()
