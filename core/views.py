# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404

from django.template import RequestContext

from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404

from django.views.decorators.cache import never_cache

from django.core.paginator import Paginator
from django.core.paginator import InvalidPage
from django.core.paginator import EmptyPage

from django.core.mail import EmailMessage

from django.db import IntegrityError

from core.forms import Newsletter
from core.forms import FeedRegister
from core.forms import ContactForm

from config.models import Setup

from core.models import News
from core.models import Subpage
from core.models import NewsFeed
from core.models import Subscriber

from itertools import chain

from hashlib import sha1

from django.conf import settings

from core.utils import renderCapchaImage

def showPosts(request, number = "1"):
    paginator = Paginator(News.objects.filter(availability = True).order_by("-date"), Setup.objects.get_or_create(id = 1)[0].page_paginator)
    config = Setup.objects.get_or_create(id = 1)[0]

    try:
        posts = paginator.page(int(number))

    except (EmptyPage, InvalidPage, ValueError):
        return HttpResponseRedirect("/planeta/news/1/")

    return render_to_response("index.html", {"posts" : posts, "truncatewords" : config.truncatewords}, context_instance = RequestContext(request))

def showSingleNews(request, slug, pk):
    post = News.objects.get(url = u"%s-%s.html" % (slug, pk))

    if post.availability:
        return render_to_response("news.html", {"post" : post}, context_instance = RequestContext(request))
    else:
        raise Http404

def showSubpage(request, subpage):
    site = get_object_or_404(Subpage, url = subpage)
    token_error = None

    if site.selectedModule == 0:
        feedRegisterForm = FeedRegister()

        if request.POST:
            feedRegisterForm = FeedRegister(request.POST)

            if feedRegisterForm.is_valid():
                token_subpage_id = request.session.get("token_subpage_%s" % site.id, default = "")

                if sha1(feedRegisterForm.cleaned_data["token"]).hexdigest() == token_subpage_id:
                    author = feedRegisterForm.cleaned_data["author"]
                    email = feedRegisterForm.cleaned_data["email"]
                    source = feedRegisterForm.cleaned_data["source"]

                    try:
                        newNewsFeed = NewsFeed(author = author, email = email, source = source)
                        newNewsFeed.save()
    
                        return HttpResponseRedirect("/planeta/zglos-bloga/sukces/")
                    except IntegrityError:
                        return HttpResponseRedirect("/planeta/zglos-bloga/error-uniq/")
                    except:
                        return HttpResponseRedirect("/planeta/zglos-bloga/error/")
                else:
                    token_error = u"Nieprawidłowy token."

            data = request.POST.copy()
            data["token"] = u""
            setattr(feedRegisterForm, "data", data)

        return render_to_response("subpage.html", {"site" : site, "feedRegisterForm" : feedRegisterForm, "token_error" : token_error}, context_instance = RequestContext(request))
    elif site.selectedModule == 1:
        contactForm = ContactForm()

        if request.POST:
            contactForm = ContactForm(request.POST)

            if contactForm.is_valid():
                token_subpage_id = request.session.get("token_subpage_%s" % site.id, default = "")

                if sha1(contactForm.cleaned_data["token"]).hexdigest() == token_subpage_id:
                    author = contactForm.cleaned_data["author"]
                    email = contactForm.cleaned_data["email"]
                    topic = contactForm.cleaned_data["topic"]
                    message = contactForm.cleaned_data["message"].replace("\r\n", "<br>")

                    ip_address = request.META.get("HTTP_X_FORWARDED_FOR", "") or request.META.get("REMOTE_ADDR")
                    host_name = request.META.get("REMOTE_HOST", "")

                    try:
                        message = u'<strong>Imię i nazwisko/Nick:</strong> %s<br><strong>E-mail:</strong> <a href="mailto:%s">%s</a><br><br><strong>Temat:</strong> %s<br><br><strong>Wiadomość:</strong><br>%s' % (author, email, email, topic, message)
                        message += '<br><br><strong>Host:</strong> %s<br><strong>IP:</strong> <a title="geoiptool.com" href="http://www.geoiptool.com/en/?%s">%s</a>' % (host_name if host_name else "nieustalony", ip_address, ip_address)

                        mail = EmailMessage(u"pl.python.org/planeta - formularz kontaktowy", message, u"%s <%s>" % (author, email), [settings.ADMINS[0][1]], headers = {"Reply-To": "%s <%s>" % (author, email)})
                        mail.content_subtype = "html"
                        mail.send()
    
                        return HttpResponseRedirect("/planeta/kontakt/sukces/")
                    except:
                        return HttpResponseRedirect("/planeta/kontakt/error/")
                else:
                    token_error = u"Nieprawidłowy token."

            data = request.POST.copy()
            data["token"] = u""
            setattr(contactForm, "data", data)

        return render_to_response("subpage.html", {"site" : site, "contactForm" : contactForm, "token_error" : token_error}, context_instance = RequestContext(request))
    else:
        return render_to_response("subpage.html", {"site" : site}, context_instance = RequestContext(request))

def showNewsletterForm(request):
    if request.POST:
        newsletter = Newsletter(request.POST)

        if newsletter.is_valid():
            validation = True

            email = newsletter.cleaned_data["email"]

            try:
                subscriber = Subscriber(email = email)
                subscriber.save()
            except IntegrityError:
                return HttpResponseRedirect("/planeta/newsletter/error/")

            title = u"pl.python.org/planeta - potwierdź chęć otrzymywania newslettera"
            text = u"Witaj,<br><br>Kłania się załoga z <a href=\"http://pl.python.org\" title=\"Polish Python Coders Group\">Polish Python Coders Group</a>.<br><br>Widzimy, że zapisałeś się do nas na newsletter <a href=\"http://pl.python.org/planeta\" title=\"Planeta polskich programistów Python\">planety</a> i chcemy Cię prosić o potwierdzenie.<br><br>W tym celu wystarczy kliknąć ten <a href=\"%snewsletter/%s\" title=\"link aktywacyjny\">link aktywacyjny</a>.<br><br>Jeśli nie zamawiałeś newslettera, to proszę zignoruj tę wiadomość.<br><br>Jeśli masz jakieś pytania, możesz do nas śmiało napisać.<br><br>Z poważaniem,<br>Polish Python Coders Group<br><a href=\"mailto:ppcg@pl.python.org\" title=\"ppcg@pl.python.org\">ppcg@pl.python.org</a>" % (settings.DOMAIN_NAME, subscriber.activation_key)
            who = u"PPCG Planeta <planeta@pl.python.org>"

            msg = EmailMessage(title, text, who, [email])
            msg.content_subtype = "html"
            msg.send()

            newsletter = Newsletter()
        else:
            validation = False
    else:
        return HttpResponseRedirect("/planeta")

    return render_to_response("newsletter.html", {"newsletter" : newsletter, "validation" : validation}, context_instance = RequestContext(request))

def showSearchResults(request, page = "1"):
    phrase = request.GET.get("fraza", u"").strip()
    config = Setup.objects.get_or_create(id = 1)[0]
    amount_of_results = 0

    if phrase:
        news = News.objects.filter(availability = True).order_by("-date")
        news_results = list(chain(news.filter(title__contains = phrase) | news.filter(text__contains = phrase)))

        amount_of_results = len(news_results)
        paginator = Paginator(news_results, Setup.objects.get_or_create(id = 1)[0].page_paginator)

        try:
            results = paginator.page(int(page))

        except (EmptyPage, InvalidPage, ValueError):
            return HttpResponseRedirect("/planeta/szukaj/1/?fraza=")

    else:
        results = []

    return render_to_response("search.html", {"results" : results, "amount_of_results" : amount_of_results, "phrase" : phrase, "truncatewords" : config.truncatewords}, context_instance = RequestContext(request))

def confirmNewsletter(request, activation_key):
    subscriber = get_object_or_404(Subscriber, activation_key = activation_key)

    if not subscriber.confirmation:
        subscriber.confirmation = True
        subscriber.save()

        return HttpResponseRedirect("/planeta/newsletter/sukces/")
    else:
        return HttpResponseRedirect("/planeta/newsletter/error/")

def showRegisterNewsletterSuccessfully(request):
    return render_to_response("newsletter.html", {"confirmNewsletterRegister" : "yes"}, context_instance = RequestContext(request))

def showRegisterNewsletterUnsuccessfully(request):
    return render_to_response("newsletter.html", {"confirmNewsletterRegister" : "no"}, context_instance = RequestContext(request))

def showRegisterFeedSuccessfully(request):
    return render_to_response("subpage.html", {"confirmFeedRegister" : "yes"}, context_instance = RequestContext(request))

def showRegisterFeedUnsuccessfully(request):
    return render_to_response("subpage.html", {"confirmFeedRegister" : "no"}, context_instance = RequestContext(request))

def showRegisterFeedUnsuccessfullyUniq(request):
    return render_to_response("subpage.html", {"confirmFeedRegister" : "uniq"}, context_instance = RequestContext(request))

def showContactFormSuccessfully(request):
    return render_to_response("subpage.html", {"contactForm" : "yes"}, context_instance = RequestContext(request))

def showContactFormUnsuccessfully(request):
    return render_to_response("subpage.html", {"contactForm" : "no"}, context_instance = RequestContext(request))

def error500(request):
    return render_to_response("subpage.html", {"error" : "500"}, context_instance = RequestContext(request))

def error404(request):
    return render_to_response("subpage.html", {"error" : "404"}, context_instance = RequestContext(request))

@never_cache
def captchaGenerator(request, url):
    setup = Setup.objects.get_or_create(id = 1)[0]
    captcha = renderCapchaImage(setup.captcha_width, setup.captcha_height, setup.captcha_length, setup.captcha_font_size)

    response = HttpResponse()
    response["Content-Type"] = "image/png"
    response.write(captcha["picture"])

    request.session["token_%s" % url] = captcha["imghash"]

    return response
