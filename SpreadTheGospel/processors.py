# -*- coding: utf-8 -*-

from urlparse import urlparse

from core.models import NewsFeed
from core.forms import SearchForm 

from menu.models import Menu

from config.models import Setup

def showAvailableFeedsAuthors(request):
    feeds = NewsFeed.objects.filter(availability = True)

    for feed in feeds:
        feed.source = urlparse(feed.source).hostname

    return {"feeds" : feeds}

def showMenu(request):
    return {"menus" : Menu.objects.all().order_by("key")}

def showSearchForm(request):
    return {"search" : SearchForm()}

def showDefaultMeta(request):
    data = Setup.objects.get_or_create(id = 1)

    return {"config" : data[0]}