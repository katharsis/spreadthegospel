# -*- coding: utf-8 -*-

#DEBUG = True
DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (("", ""),)
MANAGERS = ADMINS

EMAIL_USE_TLS = True
EMAIL_HOST = ""
EMAIL_PORT = 0
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""

MySQL_PPCG_FORUM_HOST = ""
MySQL_PPCG_FORUM_PORT = 0
MySQL_PPCG_FORUM_USER = ""
MySQL_PPCG_FORUM_PASSWORD = ""
MySQL_PPCG_FORUM_DB = ""

DOMAIN_NAME = ""

ROOT_DIR = ""

DATABASES = {
    "default": {
    "ENGINE" : "",
    "NAME" : "",
    "USER" : "",
    "PASSWORD" : "",
    "HOST" : "",
    "PORT" : "",
    "OPTIONS" : {"autocommit" : True,},
    }
}

TIME_ZONE = "Europe/Warsaw"
LANGUAGE_CODE = "pl"

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = False

MEDIA_ROOT = "%smedia/" % ROOT_DIR
MEDIA_URL = "/media/"

STATIC_ROOT = "%sstatics/" % ROOT_DIR
STATIC_URL = "/static/"

STATIC_FONT = "%sfonts/Istok-Web-Regular.ttf" % STATIC_ROOT

STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

SECRET_KEY = ""

TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.auth.context_processors.auth",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "SpreadTheGospel.processors.showAvailableFeedsAuthors",
    "SpreadTheGospel.processors.showMenu",
    "SpreadTheGospel.processors.showSearchForm",
    "SpreadTheGospel.processors.showDefaultMeta",
)

MIDDLEWARE_CLASSES = (
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.gzip.GZipMiddleware",
)

ROOT_URLCONF = "SpreadTheGospel.urls"

WSGI_APPLICATION = "SpreadTheGospel.wsgi.application"

STATIC_TEMPLATES = "%sstatics/" % ROOT_DIR

TEMPLATE_DIRS = (
    "%score/templates" % ROOT_DIR
)

INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sitemaps",
    "grappelli",
    "django.contrib.admin",
    "config",
    "core",
    "menu",
)