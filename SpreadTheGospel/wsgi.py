import os
import sys
import site

site.addsitedir("")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SpreadTheGospel.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
