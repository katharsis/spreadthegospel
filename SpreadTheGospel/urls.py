# -*- coding: utf-8 -*-

from django.conf.urls import patterns
from django.conf.urls import include
from django.conf.urls import url

from django.conf import settings

from django.contrib import admin
admin.autodiscover()

handler500 = "core.views.error500"
handler404 = "core.views.error404"

urlpatterns = patterns("",
    url(r"static/(?P<path>.*)$", "django.views.static.serve", {"document_root" : settings.STATIC_TEMPLATES}),
    url(r"^statics/(?P<path>.*)$", "django.views.static.serve", {"document_root": settings.STATIC_ROOT}, name = "statics"),
    url(r"^admin/menu/menu/move_up/(?P<menu_id>[\d]+)$", "menu.views.menu_move_up"),
    url(r"^admin/menu/menu/move_down/(?P<menu_id>[\d]+)$", "menu.views.menu_move_down"),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^sitemap", include("sitemap.urls")),
    url(r"^robots\.txt$", include("robots.urls")),
    url(r"^grappelli/", include("grappelli.urls")),
    url(r"", include("core.urls")),
)
