# SpreadTheGospel

Planet engine written in [Python language](http://python.org) and [Django framework](https://www.djangoproject.com/).  
  
It's more than a typical planet feed reader. SpreadTheGospel gives you:

* basic blog platfrom (PA based on Django admin, preferences, subpages (standard, feed submit, contact form), navigation, WYSIWYG editor, pagination),
* news archive with unique www address for each,
* built-in search engine,
* optional newsletter,
* moderator mode for news feed and subscribers.

Templates and planet feed are [W3C](http://www.w3.org/) and [WCAG 2.0](http://www.w3.org/TR/WCAG20/) (level A and AA) valid. Integrated Robots and Sitemap application provides better results for SEO/SEM.

## Requirements

* [Python 2.6](http://python.org/) (or a higher release of 2.x).
* [Django 1.4.3](https://www.djangoproject.com/) (or a 1.4.x release).
* [PIL 1.1.7](http://www.pythonware.com/products/pil/) or [Pillow 1.7.8](http://pypi.python.org/pypi/Pillow/).
* [feedparser 5.1.3](http://pypi.python.org/pypi/feedparser/).
* [django-grappelli 2.4.3](https://github.com/sehmaschine/django-grappelli).
* [MySQLdb 1.2.3](http://pypi.python.org/pypi/MySQL-python/1.2.3) (if you want to integrate newsletter with Python Job Board based on MySQL).
* [zlib 1.2.3](http://www.zlib.net/) (or a higher).

SpreadTheGospel is [PyPy](http://pypy.org/) compatible, but tested only on developer environments.

## Installation

Configure settings.py and wsgi.py file and update crontab with PlanetUpdater, JobOfferUpdater and NewsletterSend task (for that use manage.py).

## Cron

* PlanetUpdater.py - downloads news feeds and updates Planet main page.
* JobOfferUpdater.py - downloads jobs offer from board.
* NewsletterSend.py - combines actual part of news feeds and jobs offer and send newsletter to users saved in database.

## Databases
We recommend using [PostgreSQL](http://www.postgresql.org/) or [MySQL](http://www.mysql.com/).

## License

[WTFPL](http://en.wikipedia.org/wiki/WTFPL).