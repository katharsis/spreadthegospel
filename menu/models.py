# -*- coding: utf-8 -*-

from django.db import models

from core.utils import _slugify

class AbstractMenu(models.Model):
    key = models.IntegerField(verbose_name = u"Klucz", editable = False, blank = True, null = True)
    name = models.CharField(verbose_name = u"Nazwa", max_length = 150, unique = True)
    url = models.SlugField(verbose_name = u"Adres", max_length = 150, editable = False)
    href = models.CharField(verbose_name = u"Href", max_length = 150, blank = True, null = True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.url = u"/%s" % _slugify(self.name)

        if not self.pk:
            super(AbstractMenu, self).save(*args, **kwargs)

            self.key = self.pk

        if self.href:
            self.url = self.href

        super(AbstractMenu, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        [[obj.delete() for obj in rel_manager.model.objects.filter(parent = self)] for rel_manager in self._meta.get_all_related_objects()]

        super(AbstractMenu, self).delete()

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return unicode(self.name)

class Menu(AbstractMenu):
    description = models.CharField(verbose_name = u"Opis", max_length = 150, blank = True, null = True)

    class Meta:
        verbose_name = u"menu i submenu"
        verbose_name_plural = u"menus i submenus"

    def save(self, *args, **kwargs):
        if not self.description:
            self.description = self.name

        super(Menu, self).save(*args, **kwargs)

    def move(self):
        all_menus = [menu.key for menu in Menu.objects.all()]
        all_menus.sort()

        if len(all_menus) > 1:
            if self.key == min(all_menus):
                return "<a title='Do dołu' href='../menu/move_down/%d'><img alt='Do dołu' title='Do dołu' src='/statics/images/arrow-down.png'></a>" % self.key
            elif self.key == max(all_menus):
                return "<a title='Do góry' href='../menu/move_up/%d'><img alt='Do góry' title='Do góry' src='/statics/images/arrow-up.png'></a>" % self.key
            else:
                return "<a title='Do góry' href='../menu/move_up/%d'><img alt='Do góry' title='Do góry' src='/statics/images/arrow-up.png'></a> <a title='Do dołu' href='../menu/move_down/%d'><img alt='Do dołu' title='Do dołu' src='/statics/images/arrow-down.png'></a>" % (self.key, self.key)

        return ""

    move.allow_tags = True
    move.short_description = u"Zmiana kolejności"
