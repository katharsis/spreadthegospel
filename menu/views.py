# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect

from menu.models import Menu

from core.utils import template_objects_move
from core.utils import preparation_objects_to_move

def menu_move_up(request, menu_id):
    obj_with_id = preparation_objects_to_move(qs = Menu, pk = menu_id)
    clicked_key_index = obj_with_id[1]

    if clicked_key_index > 0:
        template_objects_move("up", obj_with_id[0], clicked_key_index, menu_id, Menu)

    return HttpResponseRedirect("../")

def menu_move_down(request, menu_id):
    obj_with_id = preparation_objects_to_move(qs = Menu, pk = menu_id)
    clicked_key_index = obj_with_id[1]

    if clicked_key_index < len(obj_with_id[0]) - 1:
        template_objects_move("down", obj_with_id[0], clicked_key_index, menu_id, Menu)

    return HttpResponseRedirect("../")