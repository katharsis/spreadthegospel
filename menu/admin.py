# -*- coding: utf-8 -*-

from django.contrib import admin

from menu.models import Menu

def deleteSelectedObjects(modeladmin, request, queryset):
    for menu in queryset: menu.delete()

    numberOfDeleted = len(queryset)

    info = u"Usunięto %d menu." % numberOfDeleted

    modeladmin.message_user(request, info)

deleteSelectedObjects.short_description = u"Usuń wybrane menu i submenu"

class Menu_Admin(admin.ModelAdmin):
    list_display = ("name", "url", "move")

    search_fields = ("name",)

    actions = [deleteSelectedObjects]

    def queryset(self, request):
        qs = super(Menu_Admin, self).queryset(request)

        return qs.order_by("key")

admin.site.register(Menu, Menu_Admin)
