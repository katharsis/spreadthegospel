# -*- coding: utf-8 -*-

from django.db import models

class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

class Setup(SingletonModel):
    title = models.CharField(verbose_name = u"Tytuł", max_length = 150, blank = True, null = True)
    description = models.CharField(verbose_name = u"Opis", max_length = 150, blank = True, null = True)
    keywords = models.CharField(verbose_name = u"Słowa kluczowe", max_length = 150, blank = True, null = True)

    page_paginator = models.PositiveSmallIntegerField(verbose_name = u"Paginacja", default = 5)
    truncatewords = models.PositiveSmallIntegerField(verbose_name = u"Obcinanie treści", default = 100)

    captcha_height = models.PositiveSmallIntegerField(verbose_name = u"Wysokość", default = 30)
    captcha_width = models.PositiveSmallIntegerField(verbose_name = u"Szerokość", default = 120)
    captcha_length = models.PositiveSmallIntegerField(verbose_name = u"Ilość znaków", default = 8)
    captcha_font_size = models.PositiveSmallIntegerField(verbose_name = u"Rozmiar czcionki", default = 18)

    update_data = models.DateTimeField(verbose_name = u"Data ostatniej aktualizacji", blank = True, null = True)

    newsletter = models.BooleanField(verbose_name = u"Dostępność newslettera", default = True)

    class Meta:
        verbose_name = u"preferencje"
        verbose_name_plural = u"preferencje"
