# -*- coding: utf-8 -*-

from django.contrib import admin

from config.models import Setup

from django.http import HttpResponseRedirect

from django.utils.functional import update_wrapper

from django.conf.urls.defaults import patterns
from django.conf.urls.defaults import url

class SingletonModelAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def get_urls(self):
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.module_name

        urlpatterns = patterns("",
            url(r"^history/$", wrap(self.history_view), {"object_id": "1"}, name = "%s_%s_history" % info),
            url(r"^$", wrap(self.change_view), {"object_id": "1"}, name = "%s_%s_changelist" % info),
        )

        return urlpatterns

    def response_change(self, request, obj):
        msg = u"Config został poprawnie zapisany."

        if request.POST.has_key("_continue"):
            self.message_user(request, u"Config został poprawnie zapisany. Kontynuuj edycję.")

            return HttpResponseRedirect(request.path)
        else:
            self.message_user(request, msg)

            return HttpResponseRedirect("../../")

    def change_view(self, request, object_id, extra_context = None):
        if object_id == "1":
            self.model.objects.get_or_create(pk = 1)

        return super(SingletonModelAdmin, self).change_view(request, object_id, extra_context = extra_context)

class Setup_Admin(SingletonModelAdmin):
    actions = ["delete_selected"]

    fieldsets = (
        (u"Metadane głównego szablonu", {"fields" : ("title", "description", "keywords")}),
        (u"Paginator stron/Obcinanie treści", {"fields" : ("page_paginator", "truncatewords")}),
        (u"Wymiary obrazka CAPTCHA", {"fields" : ("captcha_height", "captcha_width", "captcha_length", "captcha_font_size")}),
        (u"Ostatnia aktualizacja", {"fields" : ("update_data", )}),
        (u"Newsletter", {"fields" : ("newsletter", )}),
    )

    def has_delete_permission(self, request, obj = None):
        return False

admin.site.register(Setup, Setup_Admin)
