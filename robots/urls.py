# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import patterns
from django.conf.urls import url
from django.http import HttpResponse

urlpatterns = patterns("",
    url(r"", lambda r: HttpResponse("User-agent: *\nDisallow:\nSitemap: %ssitemap.xml\nSitemap: %ssitemap/news.xml" % (settings.DOMAIN_NAME, settings.DOMAIN_NAME), mimetype="text/plain"))
)