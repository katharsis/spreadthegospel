# -*- coding: utf-8 -*-

from django.conf.urls import patterns
from django.conf.urls import url

from django.contrib.sitemaps import GenericSitemap

from core.models import Subpage
from core.models import News

info_dict_subpage = {
    "queryset": Subpage.objects.all(),
    "date_field": "date",
}

info_dict_news = {
    "queryset": News.objects.filter(availability = True).order_by("-date"),
    "date_field": "date",
}

sitemaps_subpage = {
    "subpage": GenericSitemap(info_dict_subpage, priority = 0.6, changefreq = "daily"),
}

sitemaps_news = {
    "news": GenericSitemap(info_dict_news, priority = 0.8, changefreq = "daily"),
}

urlpatterns = patterns("",  
    url(r"^\.xml$", "django.contrib.sitemaps.views.sitemap", {"sitemaps": sitemaps_subpage}),
    url(r"^/news.xml$", "django.contrib.sitemaps.views.sitemap", {"sitemaps": sitemaps_news})
)
